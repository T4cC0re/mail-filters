# GMail Filters

This repo uses [gmailctl](https://github.com/mbrt/gmailctl) in order to
manage filters and labels in a declarative format using jsonnet. For more
information on how to set this up and the config format, please check out
the GitHub repo. Below I've included info on how to set it up for use in
GitLab CI.

## CI Setup
In order to set this up for CI you will need to do some local manual configuration first in order to get the proper credentials. It is
possible the token will expire from time to time and the steps may
need to be repeated at some point in the future.

### Local install and configure
You'll need to install `gmailctl` locally first. You will need to have
your `$GOPATH` setup prior to installing.

 ```
go get -u github.com/mbrt/gmailctl/cmd/gmailctl
go install github.com/mbrt/gmailctl/cmd/gmailctl
gmailctl init
```

When you run `gmailctl init` for the first time, you will be
presented with the directions below. Please follow them.

```
The credentials are not initialized.

To do so, head to https://console.developers.google.com

1. Create a new project if you don't have one
1. Go to 'Enable API and services' and select Gmail
2. Go to credentials and create a new one, by selecting 'Help me choose'
   2a. Select the Gmail API
   2b. Select 'Other UI'
   2c. Access 'User data'.
3. Go to 'OAuth constent screen' and update 'Scopes for Google API', by
   adding:
     * https://www.googleapis.com/auth/gmail.labels
     * https://www.googleapis.com/auth/gmail.metadata
     * https://www.googleapis.com/auth/gmail.settings.basic
4. IMPORTANT: you don't need to submit your changes for verification, as
   you're not creating a public App
5. Download the credentials file into '$HOME/.gmailctl/credentials.json'
   and execute the 'init' command again.

Documentation about Gmail API authorization can be found
at: https://developers.google.com/gmail/api/auth/about-auth
```

Once you do this, you should have template configurations and both a `$HOME/.gmailctl/credentials.json` and a `$HOME/.gmailctl/token.json`.
These are the required files authenticate.

### CI Configuration
In order to have CI authenticate for you, you'll need to get the
above credentials into CI. I've done this using the "File" variable
type provided in GitLab and then copied those files into the appropriate
location in a `before_script`. Once you have the files, you can pretty
much use almost the same CI config as I do here.
