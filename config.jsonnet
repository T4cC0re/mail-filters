// Import the standard library
local lib = import 'gmailctl.libsonnet';

local GitLab = {
  LabelInternal():: {
    filter: {
      query: "from: '*@gitlab.com' from: '*@*.gitlab.com'",
    },
    actions: {
      labels: [
        'source/internal',
      ],
    }
  },

  LabelExternal():: {
    filter: {
      query: "-label:source/internal",
    },
    actions: {
      labels: [
        'source/external',
      ],
    }
  },

  Inbox(prefix):: {
    filter: {
      to: '%s@gitlab.com' % [prefix],
    },
    actions: {
      labels: [
        'Inboxes/%s' % [prefix]
      ]
    }
  },

  OpsContact(emailSuffix, labelSuffix=''):: {
    filter: {
      to: 'ops-contact+%s@gitlab.com' % [emailSuffix],
    },
    actions: {
      labels: [
        if labelSuffix != '' then 'OpsContact/%s' % [labelSuffix] else 'OpsContact/%s' % [emailSuffix],
        'ops-contact-labeled',
      ],
    },
  },

  OpsContactCatchall():: {
    filter: {
      query: 'to:ops-contact@gitlab.com -label:ops-contact-labeled',
    },
    actions: {
      labels: [
        'OpsContact',
        'ops-contact-labeled',
      ],
    },
  },

  LabelInstance(instance):: {
    filter: {has: 'You\'re receiving this email because of your account on %s' % [instance]},
    actions: {
      labels: [
        'Instances/%s' % [instance],
        'instance-labeled'
      ],
    },
  },

  Project(path, catchall=false):: {
    filter: if catchall then {
      query: '"You\'re receiving this email because of your account on" +list:\'%s/\' -label:project-labeled -list:ops-contact.gitlab.com' % [path],
    } else {
      query: '"You\'re receiving this email because of your account on" +list:\'%s/\' -list:ops-contact.gitlab.com' % [path],
    },
    actions: {
      labels: [
        'Projects/%s' % [path],
        'project-labeled',
      ],
    },
  },

  MarkLabelRead(label, archive=true):: {
    filter: {
      query: 'label: %s' % [label],
    },
    actions: if archive then {
      archive: true,
      markRead: true,
    } else {
      markRead: true,
    },
  },
};

local hmeyer = 'hneyer@gitlab.com';
local t4cc0re = 't4cc0re@gitlab.com';
local hendrik = 'hendrik@gitlab.com';
local me = {
  or: [
    {to: hmeyer},
    {to: t4cc0re},
    {to: hendrik},
  ]
};

local rules = [
  // Any catchalls must come last!
  GitLab.LabelInternal(),
  GitLab.LabelExternal(),

  GitLab.LabelInstance('gitlab.com'),
  GitLab.LabelInstance('staging.gitlab.com'),
  GitLab.LabelInstance('ops.gitlab.net'),
  GitLab.LabelInstance('dev.gitlab.org'),
  GitLab.LabelInstance('pre.gitlab.com'),

  GitLab.OpsContact('aws', 'Amazon'),
  GitLab.OpsContact('route53', 'Amazon'),
  GitLab.OpsContact('letsencrypt', 'LetsEncrypt'),
  GitLab.OpsContact('gandi', 'Gandi'),
  GitLab.OpsContact('macstadium', 'MacStadium'),
  GitLab.OpsContact('mailgun', 'MailGun'),
  GitLab.OpsContact('deadmanssnitch', 'DeadMansSnitch'),
  GitLab.OpsContact('cloudflare', 'Cloudflare'),
  GitLab.OpsContactCatchall(),

  GitLab.Project('gitlab-com/gl-infra/infrastructure'),
  GitLab.Project('gitlab-com/gl-infra/cloudflare-audit-log'),
  GitLab.Project('gitlab-com/gl-infra/cloudflare-audit'),
  GitLab.Project('gitlab-com/gl-infra/certificates-updater'),
  GitLab.Project('gitlab-com/gl-infra/cloudflare-firewall'),
  GitLab.Project('gitlab-com/gl-infra/delivery'),
  GitLab.Project('gitlab-com/gl-infra/deployer'),
  GitLab.Project('gitlab-com/gl-infra/gitlab-web-debugger'),
  GitLab.Project('gitlab-com/gl-infra/feature-flag-log'),
  GitLab.Project('gitlab-com/gl-infra/helicopter'),
  GitLab.Project('gitlab-com/gl-infra/marquee-account-alerts'),
  GitLab.Project('gitlab-com/gl-infra/on-call-handovers'),
  GitLab.Project('gitlab-com/gl-infra/woodhouse'),
  GitLab.Project('gitlab-com/gl-infra/production'),
  GitLab.Project('gitlab-com/gl-infra/terraform-modules/cf_allowlists'),
  GitLab.Project('gitlab-com/gl-infra/terraform-modules/dns-record'),
  GitLab.Project('gitlab-com/gitlab-com-infrastructure'),
  GitLab.Project('gitlab-cookbooks/chef-repo'),
  GitLab.Project('gitlab-com/runbooks'),
  GitLab.Project('gitlab-com/www-gitlab-com'),
  GitLab.Project('gitlab-org/gitlab'),

  // Catchalls
  GitLab.Project('gitlab-com/gl-infra/terraform-modules', true),
  GitLab.Project('gitlab-com/gl-infra', true),
  GitLab.Project('gitlab-cookbooks', true),
  GitLab.Project('gitlab-com', true),
  GitLab.Project('gitlab-org', true),
  GitLab.Project('t4cc0re', true),

  GitLab.Inbox('t4cc0re'),
  GitLab.Inbox('hendrik'),
  GitLab.Inbox('hmeyer'),
  GitLab.Inbox('ops-notifications'),

  GitLab.MarkLabelRead('OpsContact/LetsEncrypt'),
  GitLab.MarkLabelRead('Projects/gitlab-com/gl-infra/feature-flag-log'),


  {
    filter: {
      query: "cloudflare.com"
    },
    actions: {
      star: true,
      markSpam: false,
      markImportant: true,
      category: "personal",
      labels: [
        "Vendors/Cloudflare"
      ]
    }
  },
  {
    filter: {
      from: "o365mc@microsoft.com"
    },
    actions: {
      markSpam: false,
      labels: [
        "Vendors/Azure"
      ]
    }
  },
  {
    filter: {
      or: [
        {from: "no-reply@pagerduty.com"},
        {from: "notifications@pagerduty.com"},
      ],
    },
    actions: {
      markSpam: false,
      markImportant: true,
      category: "personal",
      labels: [
        "Vendors/PagerDuty"
      ]
    }
  },
  {
    filter: {
      or: [
        {from: "sentry@mg.gitlab.com"},
      ],
    },
    actions: {
      archive: true,
      markSpam: false,
      markImportant: false,
      markRead: true,
      category: "updates",
      labels: [
        "Misc/Sentry"
      ]
    }
  },
  {
    filter: {
      or: [
        {to: "root@dev.gitlab.org"},
      ],
    },
    actions: {
      archive: true,
      markSpam: false,
      markImportant: false,
      markRead: true,
      category: "updates",
      labels: [
        "Misc/Crons"
      ]
    }
  },
  {
    filter: {
      query: "label:project-labeled list:ops-contact.gitlab.com",
    },
    actions: {
      archive: true,
      markSpam: false,
      markImportant: false,
      markRead: true,
      category: "updates",
      labels: [
        "OpsContact/project-updates"
      ],
    },
  },
  {
    filter: {
      or: [
        {has: "Invitation from Google Calendar"},
        {has: "iCal"},
      ],
    },
    actions: {
      star: true,
      markSpam: false,
      markImportant: true,
      category: "personal",
      labels: [
        "Meetings"
      ]
    }
  },
];

// Manual labels not referenced in rules.
local labels = [
  '_todo/follow-up',
  '_todo/task',
  '_todo/research',
];

{
  version: 'v1alpha3',
  author: {
    name: 'Hendrik Meyer',
    email: t4cc0re,
  },
  labels: lib.rulesLabels(rules) + [{ name: l } for l in labels],
  rules: rules,
}
